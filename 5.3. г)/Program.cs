﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5._3.г_
{
    class Program
    {
        static void Main(string[] args)
        {
            int i;
            int StartI;
            int FinishI;

            Console.Write("Введите начальное значение: ");
            StartI = int.Parse(Console.ReadLine());

            Console.Write("Введите заключительное значение: ");
            FinishI = int.Parse(Console.ReadLine());

            i = StartI;
            while (i <= FinishI)
            {
                Console.WriteLine(i);
                i++;
            }
            Console.ReadKey();
        }
    }
}
